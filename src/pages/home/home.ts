import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { PokemonPage } from '../pokemon/pokemon';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pokemons: any[] = [];

  constructor(
    public navCtrl: NavController,
    private httpClient: HttpClient
  ) { }

  ionViewWillEnter(): void {
   this.httpClient.get('https://pokeapi.co/api/v2/pokemon/?offset=0&limit=151').subscribe((result: any) => {
     this.pokemons = result.results.map(pokemon => {
       pokemon.name = pokemon.name.toUpperCase();
       return pokemon;
     });
   });
  }

  onPokemonClick(pokemon: any): void {
    this.navCtrl.push(PokemonPage, {
      'pokemon': pokemon
    })
  }

}
