import { PokemonMovesPage } from './../pokemon-moves/pokemon-moves';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

/**
 * Generated class for the PokemonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pokemon',
  templateUrl: 'pokemon.html',
})
export class PokemonPage {

  base: any;
  pokemon: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private httpClient: HttpClient
    ) 
  {
    this.base = this.navParams.get('pokemon');
  }

  ionViewDidLoad() {
    const loader = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loader.present();
    this.httpClient.get(this.base.url).subscribe(pokemon$ => {
      this.pokemon = pokemon$;
      loader.dismiss();
    });
  }

  openPokemonMoves(): void {
    this.navCtrl.push(PokemonMovesPage, {
      'moves': this.pokemon.moves
    });
  }

}
