import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MovePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-move',
  templateUrl: 'move.html',
})
export class MovePage {

  move: any;
  moveInfo: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private httpClient: HttpClient) {
    this.move = this.navParams.get('move');
  }

  ionViewDidLoad(): void {
    if (!!this.move) {
      this.httpClient.get(this.move.url).subscribe(move$ => {
        this.moveInfo = move$;
      });
    }
  }

}
