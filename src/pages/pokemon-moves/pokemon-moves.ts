import { MovePage } from './../move/move';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PokemonMovesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pokemon-moves',
  templateUrl: 'pokemon-moves.html',
})
export class PokemonMovesPage {

  moves: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.moves = this.navParams.get('moves');
  }

  ionViewDidLoad() { }

  openMovePage(move: any): void {
    this.navCtrl.push(MovePage, {
      'move': move.move
    });
  }

}
