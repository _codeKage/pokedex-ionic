import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PokemonMovesPage } from './pokemon-moves';

@NgModule({
  declarations: [
    PokemonMovesPage,
  ],
  imports: [
    IonicPageModule.forChild(PokemonMovesPage),
  ],
})
export class PokemonMovesPageModule {}
